# Mi empresa de prácticas: Omnios

<p align="center">
  <img src="OmniosTitle.png" alt="Imagen Omnios" width="500">
</p>

## 1. Presentación Personal y del Instituto

<div style="background-color: #C5B0E8; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Presentación Personal:</h2>
  <p>Mi nombre es <strong>Gerard Albesa</strong> y soy estudiante del instituto Nicolau Copernic, donde actualmente estoy cursando un grado superior en desarrollo de aplicaciones multiplataforma. Durante mi período de prácticas, tuve el privilegio de colaborar con Omnios, una empresa en el sector de desarrollo de software inteligente basado en tecnología avanzada de inteligencia artificial (IA).</p>
</div>

<div style="background-color: #dfeaf5; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Presentación del Instituto:</h2>
  <p align="center">
    <img src="logoNicolauCopernic.png" alt="Imagen Nicolau Copernic" width="300">
  </p>
  <p>Mi instituto se llama Nicolau Copernic y se encuentra en el Vallés, específicamente en Terrassa. Ofrece una amplia gama de enseñanzas que van desde la ESO hasta Bachillerato, así como Ciclos Formativos de Grado Medio y Grado Superior. Entre las diversas ofertas de Grado Superior que brinda el instituto se encuentra el programa que actualmente estoy cursando, Desarrollo de Aplicaciones Multiplataforma, el cual se imparte en horario de tarde.</p>
</div>

<div style="background-color: #C5B0E8; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Motivación que me llevó a elegir</h2>
  
  <h3 style="color: #004085;">El ciclo que estoy cursando</h3>
  <p>Escogí el ciclo de Desarrollo de Aplicaciones Multiplataforma en el Nicolau Copernic porque siempre he tenido un gran interés por la tecnología y la programación. En este ciclo se ofrece el estudio de Java, un lenguaje que me interesaba aprender. Además, el enfoque en el estudio por proyectos y no por asignaturas me parece clave para prepararme adecuadamente para el mercado laboral.</p>

  <h3 style="color: #004085;">La modalidad de prácticas duales</h3>
  <p>Opté por la modalidad Dual porque consideraba que era esencial para dar un primer gran paso hacia el mercado laboral y abrirme camino en él. No solo lo veía necesario, sino también obligatorio, ya que unas prácticas de tres meses no me parecían suficientes. Sin embargo, debido a la escasez de empresas que ofrecen prácticas en modalidad Dual para los diferentes ciclos, la oferta que nos presentó el instituto no cubría la demanda de todos los alumnos que optamos por esta modalidad. Tras un fallido convenio, que finalmente decidió no contratar alumnos, me encontré en el último momento sin una empresa donde realizar mis prácticas.</p>
  
  <p>Por esta razón, me dediqué durante todo el verano e incluso al inicio de las clases a contactar con diferentes empresas, mostrando mi interés en realizar prácticas en alguna de ellas. Tras muchas entrevistas telefónicas, virtuales y presenciales, que incluían pruebas técnicas, logré obtener una gran oportunidad en mi actual empresa, Omnios.</p>

  <h3 style="color: #004085;">Objetivos que quiero alcanzar con la estancia</h3>
  <ul>
    <li>Aplicar y consolidar los conocimientos teóricos adquiridos durante el ciclo.</li>
    <li>Adquirir nuevas habilidades y competencias técnicas específicas del entorno laboral.</li>
    <li>Desarrollar habilidades profesionales, como el trabajo en equipo, la gestión del tiempo y la resolución de problemas.</li>
    <li>Contribuir de manera significativa a los proyectos de la empresa, aportando valor y demostrando mis capacidades.</li>
    <li>Establecer una primera experiencia laboral real y así abrirme al mundo laboral como desarrollador de software.</li>
  </ul>
</div>

## 2. Dades d’entitat

<div style="background-color: #dfeaf5; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Dirección de la Empresa</h2>
  <p><strong>Dirección de la Empresa:</strong> Rda. de Sant Antoni, 36, Ciutat Vella, 08001 Barcelona, España</p>
</div>

<div style="background-color: #C5B0E8; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Introducción</h2>
  <p>Durante mi período de prácticas, tuve la oportunidad de colaborar con Omnios, un referente en el sector de "Desarrollo de software inteligente basado en tecnología avanzada de inteligencia artificial (IA)".</p>
  <p>Mi trabajo se centró en el departamento de QA, donde pude contribuir al desarrollo de sistemas multiplataforma como automatizador de pruebas QA, al igual que hice de desarrollador, así de esa manera fortalecía mis habilidades técnicas y profesionales.</p>
</div>

<div style="background-color: #dfeaf5; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Descripción de la Empresa</h2>

  <h3 style="color: #004085;">Información General</h3>
  <ul>
    <li><strong>Nombre de la Empresa:</strong> Omnios</li>
    <li><strong>Sector:</strong> Tecnología de la Información y Servicios</li>
    <li><strong>Tamaño de la Empresa:</strong> Pequeña empresa (30 empleados)</li>
    <li><strong>Mercado Principal:</strong> Desarrollo de software a medida para soluciones basadas en Inteligencia Artificial</li>
  </ul>

  <h3 style="color: #004085;">Infraestructura y Herramientas</h3>
  <p>Durante mi período de prácticas en Omnios, tuve acceso a una infraestructura tecnológica que facilitó enormemente el desarrollo y la implementación de proyectos. Esta infraestructura incluía servidores dedicados para entornos de desarrollo y pruebas, lo que permitía realizar pruebas sin interferir con los sistemas en producción. Además, se nos proporcionaron herramientas específicas que fueron fundamentales para nuestro trabajo:</p>
  
  <ul>
    <li><strong>Servidores de Desarrollo y Pruebas:</strong> Estos servidores proporcionaron un entorno seguro y controlado para probar y depurar nuestras aplicaciones antes de su lanzamiento al entorno de producción. La disponibilidad de servidores separados para cada etapa del desarrollo nos permitió realizar pruebas rigurosas sin afectar a los sistemas en producción.</li>
    <li><strong>Herramientas de Gestión de Proyectos:</strong> Utilizamos Jira como nuestra principal herramienta de gestión de proyectos. En Jira, nuestro tablero estaba estructurado de la siguiente manera:
      <ul>
        <li><strong>To Do:</strong> Tareas por hacer.</li>
        <li><strong>In Progress:</strong> Tareas en progreso.</li>
        <li><strong>Code Review:</strong> Tareas en revisión de código.</li>
        <li><strong>Dev AC Test:</strong> Tareas en prueba de aceptación del desarrollo.</li>
        <li><strong>Ready for QA:</strong> Tareas listas para ser probadas por QA.</li>
        <li><strong>QA:</strong> Tareas en proceso de prueba de calidad.</li>
        <li><strong>Validated:</strong> Tareas validadas.</li>
        <li><strong>Ready to Publish:</strong> Tareas listas para ser publicadas.</li>
        <li><strong>Done:</strong> Tareas completadas y cerradas.</li>
      </ul>
    </li>
    <li><strong>Herramientas de Control de Versiones:</strong> Para el control de versiones utilizamos Bitbucket, que nos permitió trabajar con un sistema de ramificación muy similar a GitHub. Utilizamos las siguientes ramas para gestionar nuestro flujo de trabajo:
      <ul>
        <li><strong>feature:</strong> Ramas locales para el desarrollo de nuevas características.</li>
        <li><strong>bugfix:</strong> Ramas para corregir errores.</li>
        <li><strong>hotfix:</strong> Ramas para correcciones urgentes en producción.</li>
        <li><strong>development:</strong> Rama principal de desarrollo.</li>
        <li><strong>QA:</strong> Rama para pruebas de calidad.</li>
        <li><strong>prod:</strong> Rama de producción, donde se despliegan las versiones estables del software.</li>
      </ul>
    </li>
  </ul>

  <p>Estas herramientas y prácticas de desarrollo fueron fundamentales para mantener un proceso de desarrollo fluido durante mis prácticas en Omnios así como entender el flujo de trabajo y adaptarme a un entorno de trabajo real.</p>
</div>

<div style="background-color: #C5B0E8; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h3 style="color: #002e5d;">Organigrama del Departamento</h3>
  <p>Para comprender mejor el contexto en el que trabajé, se incluye a continuación un esquema del organigrama de los departamentos dentro de Omnios.</p>
  <p align="center">
    <img src="OrganigramaOmnios.jpeg" alt="Organigrama" width="500">
  </p>
</div>

## 3. Dades de l’estada

<div style="background-color: #dfeaf5; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Tipo de Contratación</h2>
  <p><strong>Tipo de contratación:</strong> Software Developer Trainee</p>
</div>

<div style="background-color: #C5B0E8; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Beneficios Sociales</h2>
  <p>Durante mi período de prácticas en Omnios, disfruté de varios beneficios sociales que mejoraron significativamente mi experiencia laboral:</p>
  <ul>
    <li><strong>Desarrollo Profesional:</strong> Se me dio la opción de dedicar un cierto número de horas a mi desarrollo profesional mediante cursos en plataformas como Udemy.</li>
    <li><strong>Trabajo Remoto:</strong> Tengo la posibilidad de trabajar de forma remota dos días a la semana y una semana entera una vez al año.</li>
    <li><strong>Horarios Flexibles:</strong> Esto me permitió ajustar mi horario de trabajo según mis necesidades personales y profesionales.</li>
    <li><strong>Equipamiento:</strong> Se me otorgó un portátil para poder realizar mi trabajo correctamente.</li>
    <li><strong>Comidas y Snacks Gratuitos:</strong> Disponemos de café, agua y fruta en la oficina.</li>
    <li><strong>Eventos Sociales:</strong> Se organizan salidas en grupo como pádel, karting, vóley y una comida con todo el equipo los viernes.</li>
  </ul>
  <p>Estos beneficios contribuyeron a crear un ambiente de trabajo positivo y a mi crecimiento profesional durante las prácticas.</p>
</div>

<div style="background-color: #dfeaf5; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Total de Horas</h2>
  <ul>
    <li><strong>Total de horas de prácticas:</strong> 1000 horas</li>
    <li><strong>Fechas de las prácticas:</strong> Del 5 de diciembre de 2023 al 1 de Junio de 2024</li>
  </ul>
</div>

## 4. Relaciones profesionales y seguimiento de la estancia

<div style="background-color: #C5B0E8; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Tutores y Valoración del Seguimiento</h2>
  <p>Durante mis prácticas, recibí orientación y apoyo continuo de mis tutores tanto en Omnios como en mi instituto. Los datos de mis tutores son los siguientes:</p>
  <ul>
    <li><strong>Tutor en Omnios:</strong> Tomas C., CTO</li>
    <li><strong>Tutor en el Instituto:</strong> Iván Cantón</li>
    <li><strong>Tutor de prácticas:</strong> Víctor Carretero</li>
  </ul>
  <p>La comunicación con mis tutores fue constante y efectiva, con reuniones semanales y revisiones de progreso periódicas.</p>
  <p>La valoración del seguimiento por parte de mis tutores fue muy positiva. Se destacó mi capacidad para adaptarme rápidamente a nuevas tareas y tecnologías, así como mi dedicación al proyecto y mi capacidad para trabajar de manera autónoma cuando fue necesario. Además, la comunicación con el resto de compañeros fue fácil y enriquecedora, lo cual ayudó a crear un ambiente de trabajo colaborativo y productivo.</p>
</div>

## 5. Diari de pràctiques

<div style="background-color: #dfeaf5; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Semana del 20 de mayo al 25 de mayo</h2>
  <ul>
    <li><strong>Proyectos y Tareas Realizadas:</strong>
      <p>Descripción de los proyectos y tareas en las que trabajé durante esta semana, incluyendo cualquier avance significativo o desafíos encontrados.</p>
    </li>
    <li><strong>Preparación del Lugar de Trabajo:</strong>
      <p>Detalles sobre la preparación del lugar de trabajo, configuración de herramientas o entornos necesarios para llevar a cabo mis tareas.</p>
    </li>
    <li><strong>Reuniones:</strong>
      <p>Resumen de las reuniones en las que participé durante la semana, incluyendo temas discutidos y acciones acordadas.</p>
    </li>
  </ul>
</div>
<div>
<div style="background-color: #C5B0E8; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <p align="center">
    <img src="TablaDesempeno.png" alt="Tabla temporal" width="500">
  </p>
</div>

<div style="background-color: #dfeaf5; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2>Gráfico actual<h2>
  <p align="center">
    <img src="graficoActual.png" alt="Grafico temporal" width="500">
  </p>
</div>
<div style="background-color: #C5B0E8; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
<h2>Gráfico de mejora<h2>
  <p align="center">
    <img src="graficoMejora.png" alt="Grafico temporal" width="500">
  </p>
<d>
</div>

## 6. Problemas surgidos al realizar la estancia dual

<div style="background-color: #C5B0E8; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <h2 style="color: #002e5d;">Problemas y Soluciones</h2>
  <ul>
    <li><strong>Problema:</strong> Falta de conocimientos en React.</li>
    <li><strong>Explicación:</strong> Durante mi período de prácticas, he necesitado obtener conocimientos para desarrollar el front de las aplicaciones.</li>
    <li><strong>Solución:</strong> Durante mi tiempo libre, me he dedicado a aprender esta tecnología además del gran apoyo de mis compañeros para ayudarme en cualquier momento que necesite.</li>
    <br>
    <li><strong>Problema:</strong> Dificultades en la comunicación.</li>
    <li><strong>Explicación:</strong> Sobre todo al principio, me costaba comunicarme cuando necesitaba ayuda; intentaba hacer las cosas por mi cuenta y eso provocaba que tardara más de lo esperado.</li>
    <li><strong>Solución:</strong> Con el tiempo y gracias al feedback de mis seniors, he conseguido comunicarme mejor y con anticipación.</li>
</ul>
</div>


## 7. Valoración personal de la estancia Dual

<div style="background-color: #dfeaf5; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <p>Mis prácticas me brindaron la oportunidad de desarrollar las siguientes habilidades y competencias:</p>
  <ul>
    <li><strong>Desarrollo de Aplicaciones Multiplataforma:</strong> Aprendí a desarrollar aplicaciones con un backend y frontend por separado utilizando el back como una API, también aprendí a usar tecnologías importantes como TypeScript, Python y React, al igual que tener un código limpio y organizado lo que amplió mi capacidad técnica.</li>
    <li><strong>Trabajo en Equipo:</strong> Colaboré estrechamente con otros desarrolladores y miembros del equipo, al igual que aprendí a utilizar herramientas de trabajo como Jira gracias a eso he mejorado mis habilidades de comunicación, colaboración y conocimientos de trabajo.</li>
    <li><strong>Resolución de Problemas:</strong> Enfrenté desafíos técnicos y aprendí a encontrar soluciones efectivas y eficientes así de esta manera mejorar mi nivel de código.</li>
    <li><strong>Gestión de Proyectos:</strong> Participé en la planificación y ejecución de proyectos utilizando metodologías ágiles, mejorando mi capacidad para gestionar proyectos de manera efectiva.</li>
  </ul>
</div>

## 8. Análisis Crítico y Personal

<div style="background-color: #C5B0E8; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <p>Durante mis prácticas, enfrenté desafíos significativos, como la gestión del tiempo y la resolución de problemas técnicos complejos. Sin embargo, estos desafíos me permitieron crecer tanto a nivel profesional como personal. Aprendí a trabajar bajo presión y a mantener un alto nivel de calidad en mi trabajo. Además, adquirí una comprensión más profunda de las tecnologías y metodologías de desarrollo de software, lo que me preparó mejor para mi futura carrera profesional en el campo del desarrollo.</p>
  <p>Gracias a mis compañeros, también mejoré mi calidad de código, recibiendo valiosos consejos y feedback que me ayudaron a desarrollar habilidades más sólidas en este aspecto.</p>
</div>

## 9. Conclusiones

<div style="background-color: #dfeaf5; padding: 20px; border-radius: 10px; margin-bottom: 20px;">
  <p>Mis prácticas fueron una experiencia invaluable no tan solo a nivel de experiencia sino que también gracias al gran equipo de Omnios donde todos me apoyaron y ayudaron desde el primer momento. Todos los trabajadores de esa empresa trabajan a un nivel increíble además del buen ambiente laboral que es lo mejor de la empresa, desde el fundador de la empresa hasta el último trabajador son unas personas maravillosas que me han hecho pasar momentos increíbles a la vez que llevábamos un nivel de exigencia alto. Solo tengo cosas positivas que decir de Omnios, estoy orgulloso de haber podido participar en este equipo. Además de que me permitió aplicar mis conocimientos teóricos en un entorno profesional. Aprendí mucho sobre desarrollo de software, trabajo en equipo y gestión de proyectos, y estoy seguro de que estas experiencias serán fundamentales para mi futuro profesional en el campo de la tecnología.</p>
</div>